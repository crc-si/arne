import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Layout from "./pages/Layout";
import Main from "./pages/Main";

const app = document.getElementById("app");

ReactDOM.render(
	<Router history={hashHistory}>
		<Route path="/" component={Layout}>
      		<IndexRoute component={Main}></IndexRoute>
		</Route>
	</Router>,
app);