import Stemmer from "stemmer";
import doubleMetaphone from 'talisman/phonetics/double-metaphone';
import {original, revisited} from 'talisman/phonetics/caverphone';
import onca from 'talisman/phonetics/onca';
import fuzzySoundex  from 'talisman/phonetics/fuzzy-soundex';
import phonex from 'talisman/phonetics/phonex';
import sonoripy from 'talisman/tokenizers/syllables/sonoripy';
import Metaphone3 from "./Metaphone3";

var regex = /^[a-zA-Z_ ]+$/;
var DoubleMetaphone = doubleMetaphone;
var CaverphoneOriginal = original;
var CaverphoneRevisited = revisited;
var Onca = onca;
var FuzzySoundex = fuzzySoundex;
var Phonex = phonex;
var Sonoripy = sonoripy;

export function regEx (roadName){
	return regex.test(roadName);
}

export function encodeRoadName(roadName){
	regex = /^[a-zA-Z_' ]+$/;
  var obj = {
    metaphonePrimary:"",
    metaphoneAlternate: "",
    stem: "",
    stemMetaphonePrimary: "",
    stemMetaphoneAlternate: "",
    caverphoneOriginal: "",
    caverphoneRevisited: "",
    onca: "",
    phonex: "",
    fuzzySoundex: "",
    sonoripyCount: "",
    sonoripyPhonetics: "",
    m3Metaph: "",
    m3MetaphAlt: "",
    m3MetaphEnEx: "",
    m3MetaphEnExAlt: "",
    m3MetaphEnExVow: "",
    m3MetaphEnExVowAlt: "",
    m3MetaphEnVow: "",
    m3MetaphEnVowAlt: ""
  }

  if(regex.test(roadName.trim())){

    obj.metaphonePrimary = DoubleMetaphone(roadName.trim())[0];
    obj.metaphoneAlternate = DoubleMetaphone(roadName.trim())[1];
    obj.stem = Stemmer(roadName.trim());
    obj.stemMetaphonePrimary = DoubleMetaphone(Stemmer(roadName.trim()))[0];
    obj.stemMetaphoneAlternate = DoubleMetaphone(Stemmer(roadName.trim()))[1];
    obj.caverphoneOriginal = CaverphoneOriginal(roadName.trim());
    obj.caverphoneRevisited = CaverphoneRevisited(roadName.trim());
    obj.onca = Onca(roadName.trim());
    obj.phonex = Phonex(roadName.trim());
    obj.fuzzySoundex = FuzzySoundex(roadName.trim());
    obj.sonoripyCount = Sonoripy(roadName.trim().toLowerCase()).length;
    obj.sonoripyPhonetics = DoubleMetaphone(Sonoripy(roadName.trim().toLowerCase())[Sonoripy(roadName.trim().toLowerCase()).length-1])[1];

    var m3 = new Metaphone3();
    // Encode DEFAULT
    m3.SetWord(roadName.trim());
    m3.Encode(); obj.m3Metaph = m3.GetMetaph(); obj.m3MetaphAlt = m3.GetAlternateMetaph();
    // Encode with EXACT MATCH
    m3.SetEncodeExact(true);
    m3.Encode(); obj.m3MetaphEnEx = m3.GetMetaph(); obj.m3MetaphEnExAlt = m3.GetAlternateMetaph();
    // Encode with EXACT MATCH AND EXACT VOWEL
    m3.SetEncodeVowels(true);
    m3.Encode(); obj.m3MetaphEnExVow = m3.GetMetaph(); obj.m3MetaphEnExVowAlt = m3.GetAlternateMetaph();
    // Encode with EXACT VOWEL
    m3.SetEncodeExact(false);
    m3.Encode(); obj.m3MetaphEnVow = m3.GetMetaph(); obj.m3MetaphEnVowAlt = m3.GetAlternateMetaph();
  }
  return obj;
}