import * as config from "../config";

var conf = config.config;
export function loadMapworks() {
  window.addEventListener("dragover",function(e){
    e = e || event;
    e.preventDefault();
  },false);
  window.addEventListener("drop",function(e){
    e = e || event;
    e.preventDefault();
  },false);

  window.path = window.location.pathname
  window._ = Studio._;
  window.map = new Studio.core.Map("#map", {apiKey: conf.apiKey,
                                        		map: conf.map,
                                        		mapworksPath: conf.mapworksPath,
                                        		mapPath: conf.mapPath,
                                                bingKey: conf.bingKey,
                                        		navigationControl: false,
                                        		appNavigationControl: false,
                                        		scaleControl: false,
                                        		toolbarControl: false,
                                        		tooltipControl: true,
                                        		zoomControl: true
                                        	}).load(function (err, map) {
                                        		window.map = map;
                                        	});
  var mapFetchFail = function (response) {
    var message = 'Server response: ' + response.status + ' ' + response.statusText + '. ' + response.responseJSON.message;
    Studio.app.component.dialogue.Dialogue.alert({
      title: 'Map fetch failed',
      message: message
    });
  };
	map.once('ready', function () {
		map.off('fetch:failed', mapFetchFail);
        // Hide default tooltip when mouse over a feature
        map.getControl('tooltip').on('before:render', function (model, tplObject) {
        	if (tplObject.mouseoverTpl) {
        		tplObject.mouseoverTpl = _.template('');
        	}
        });

        // Set all Layers as intactive
        var nodes = map.getTree().nodes();
        _.each(nodes.models, function (n) {
            var layers = _.map(n.where({type: Studio.core.entity.TreeEntity.Type.LAYER}),
                function (node) {
                    return node.getTitle();
                });
            _.each(layers, function (l) {
                var layer = map.getTree().findByTitle(l);
                layer.setActive(false);
            });
        });

        var search = new Studio.app.component.search.Search({
            map: map,
            engines: [{
                url: map.getServerUrl() + '/feature/search',
                type: Studio.app.component.search.Search.Engines.LAYER,
                layers: Studio._.pluck(map.getFilteredLayers(
                    [
                    Studio.core.entity.TreeLayerEntity.LayerType.VECTOR
                    ],
                    false,
                    true
                    ), 'id')
            }].concat(conf.bingKey ? [{
                type: Studio.app.component.search.Search.Engines.BING,
                key: conf.bingKey
            }, {
                type: Studio.app.component.search.Search.Engines.BING,
                name: 'Places',
                queryParam: 'q',
                key: conf.bingKey
            }] : [])
        }).render();

        search.$el.appendTo(map.getControlRegion('topLeft'));

        // Add Point Layer
        var pointLayer = new Studio.core.entity.TreeVectorLayerEntity({attributes: {maxScale: map.getMaxScale(),
                                                                                    minScale: map.getMinScale()},
                                                                        title: config.layer.pointLayer,
                                                                        visible: true,
                                                                        active: false,
                                                                        labelled: true,
                                                                        fields: {},
                                                                        layerFields: [{}],
                                                                        styles: {
                                                                        	'#': {
                                                                        		'default': {
                                                                                    polygonFill: "blue",
                                                                                    polygonOpacity: 0.1,
                                                                        			pointIcon: path + 'images/pin-ds-orange-20.png',
                                                                        			pointIconOpacity: 1
                                                                        		},
                                                                        		'active': {
                                                                        			pointIcon: path + 'images/pin-ds-orange-20.png',
                                                                        			pointIconOpacity: 1
                                                                        		}
                                                                        	}
                                                                        },
                                                                        source: {
                                                                        	adhoc: {}
                                                                        }
                                                                    },
                                                                    {map: map});
        map.getTree().add(pointLayer);

        // Add Line Layer
        var layerFields = {"roadName": {"name": "roadName",
                                        "title": "Road Name"},
                            "lga": {"name": "lga",
                                        "title": "LGA"},
                            "locality": {"name": "locality",
                                        "title": "Locality"},
                            "distance": {"name": "distance",
                                        "title": "Distance"}};
        var fields = [
                        {"name": "roadName",
                            send: true,
                            type: 311,
                            "id": "roadName"
                        },
                        {"name": "lga",
                            send: true,
                            type: 311,
                            "id": "lga"
                        },
                        {"name": "locality",
                            send: true,
                            type: 311,
                            "id": "locality"
                        },
                        {"name": "distance",
                            send: true,
                            type: 311,
                            "id": "distance"
                        }
                      ];
        var lineColor = "#FF5000";
        var polyLineLayer = new Studio.core.entity.TreeVectorLayerEntity({attributes: {maxScale: map.getMaxScale(),
                                                                                        minScale: map.getMinScale()},
                                                                            title: config.layer.polyLineLayer,
                                                                            visible: true,
                                                                            active: true,
                                                                            labelled: true,
                                                                            fields: layerFields,
                                                                            layerFields: [layerFields],
                                                                            styles: {
                                                                                '#': {
                                                                                    'default': {
                                                                                        lineFill: lineColor,
                                                                                        pointLineFill: lineColor,
                                                                                        polygonTextureFill: lineColor,
                                                                                        lineOpacity: 0.5,
                                                                                        pointLineOpacity: 0.5,
                                                                                        lineDash: [10, 10],
                                                                                        lineWidth: [2],
                                                                                        labelFont: "Sans-Serif",
                                                                                        labelSize: [12],
                                                                                        textSize: 2,
                                                                                        labelTemplate: "|roadName|",
                                                                                        hoverTemplate: "|roadName|"
                                                                                    },
                                                                                    'active': {
                                                                                        lineFill: lineColor,
                                                                                        pointLineFill: lineColor,
                                                                                        polygonTextureFill: lineColor,
                                                                                        lineOpacity: 1,
                                                                                        pointLineOpacity: 1,
                                                                                        lineDash: [10, 10],
                                                                                        lineWidth: [2],
                                                                                        labelTemplate: "|roadName|",
                                                                                        hoverTemplate: "|roadName|"
                                                                                    }
                                                                                }
                                                                            },
                                                                            source: {
                                                                                adhoc: {"features": {},
                                                                                          "fields": fields}
                                                                            }
                                                                        },
                                                                        {map: map});
            map.getTree().add(polyLineLayer);

        // Add Sounds Like Line Layer
        var lineColorSoundsLike = "#CC00FF";
        var polyLineLayerSoundsLike = new Studio.core.entity.TreeVectorLayerEntity({attributes: {maxScale: map.getMaxScale(),
                                                                                        minScale: map.getMinScale()},
                                                                                        title: config.layer.polyLineLayerSoundsLike,
                                                                                        visible: true,
                                                                                        active: true,
                                                                                        labelled: true,
                                                                                        fields: layerFields,
                                                                                        layerFields: [layerFields],
                                                                                        styles: {
                                                                                            '#': {
                                                                                                'default': {
                                                                                                    lineFill: lineColorSoundsLike,
                                                                                                    pointLineFill: lineColorSoundsLike,
                                                                                                    polygonTextureFill: lineColorSoundsLike,
                                                                                                    lineOpacity: 0.5,
                                                                                                    pointLineOpacity: 0.5,
                                                                                                    lineDash: [10, 10],
                                                                                                    lineWidth: [2],
                                                                                                    labelFont: "Sans-Serif",
                                                                                                    labelSize: [12],
                                                                                                    textSize: 2,
                                                                                                    labelTemplate: "|roadName|",
                                                                                                    hoverTemplate: "|roadName|"
                                                                                                },
                                                                                                'active': {
                                                                                                    lineFill: lineColorSoundsLike,
                                                                                                    pointLineFill: lineColorSoundsLike,
                                                                                                    polygonTextureFill: lineColorSoundsLike,
                                                                                                    lineOpacity: 1,
                                                                                                    pointLineOpacity: 1,
                                                                                                    lineDash: [10, 10],
                                                                                                    lineWidth: [2],
                                                                                                    labelTemplate: "|roadName|",
                                                                                                    hoverTemplate: "|roadName|"
                                                                                                }
                                                                                            }
                                                                                        },
                                                                                        source: {
                                                                                            adhoc: {"features": {},
                                                                                                      "fields": fields}
                                                                                        }
                                                                                    },
                                                                                {map: map});
        map.getTree().add(polyLineLayerSoundsLike);

        // Add May Sound Like Line Layer
        var lineColorMaySoundLike = "#FF33CC";
        var polyLineLayerMaySoundLike = new Studio.core.entity.TreeVectorLayerEntity({attributes: {maxScale: map.getMaxScale(),
                                                                                        minScale: map.getMinScale()},
                                                                                        title: config.layer.polyLineLayerMaySoundLike,
                                                                                        visible: true,
                                                                                        active: true,
                                                                                        labelled: true,
                                                                                        fields: layerFields,
                                                                                        layerFields: [layerFields],
                                                                                    styles: {
                                                                                        '#': {
                                                                                            'default': {
                                                                                                lineFill: lineColorMaySoundLike,
                                                                                                pointLineFill: lineColorMaySoundLike,
                                                                                                polygonTextureFill: lineColorMaySoundLike,
                                                                                                lineOpacity: 0.5,
                                                                                                pointLineOpacity: 0.5,
                                                                                                lineDash: [10, 10],
                                                                                                lineWidth: [2],
                                                                                                labelFont: "Sans-Serif",
                                                                                                labelSize: [12],
                                                                                                textSize: 2,
                                                                                                labelTemplate: "|roadName|",
                                                                                                hoverTemplate: "|roadName|"
                                                                                            },
                                                                                            'active': {
                                                                                                lineFill: lineColorMaySoundLike,
                                                                                                pointLineFill: lineColorMaySoundLike,
                                                                                                polygonTextureFill: lineColorMaySoundLike,
                                                                                                lineOpacity: 1,
                                                                                                pointLineOpacity: 1,
                                                                                                lineDash: [10, 10],
                                                                                                lineWidth: [2],
                                                                                                labelTemplate: "|roadName|",
                                                                                                hoverTemplate: "|roadName|"
                                                                                            }
                                                                                        }
                                                                                    },
                                                                                    source: {
                                                                                        adhoc: {"features": {},
                                                                                                  "fields": fields}
                                                                                    }
                                                                                },
                                                                                {map: map});
        map.getTree().add(polyLineLayerMaySoundLike);

        // Add Road Line Layer
        var lineColorRoad = "#FF5000";
        var polyLineLayerRoad = new Studio.core.entity.TreeVectorLayerEntity({attributes: {maxScale: 25000,
                                                                                        minScale: map.getMinScale()},
                                                                                        title: config.layer.polyLineLayerRoad,
                                                                                        visible: true,
                                                                                        active: true,
                                                                                        labelled: true,
                                                                                        fields: layerFields,
                                                                                        layerFields: [layerFields],
                                                                                    styles: {
                                                                                        '#': {
                                                                                            'default': {
                                                                                              lineFill: lineColorRoad,
                                                                                              polygonOpacity: 0.5,
                                                                                              labelFont: "Sans-Serif",
                                                                                              labelSize: [12],
                                                                                              labelTemplate: "|roadName|",
                                                                                              hoverTemplate: "|roadName|",
                                                                                              pointLineWidth: 10,
                                                                                              pointWidth: 10,
                                                                                              textSize: 10,
                                                                                              lineWidth: [4, 25000, 15, 10000],
                                                                                              pointLineFill: lineColorRoad,
                                                                                              polygonTextureOpacity: 0.5,
                                                                                              imageOpacity: 0.5,
                                                                                              pointOpacity: 0.5,
                                                                                              pointIconOpacity: 0.5,
                                                                                              lineOpacity: 0.8,
                                                                                              pointLineOpacity: 0.8,
                                                                                              lineJoin: "round",
                                                                                              lineCap: "round"
                                                                                            },
                                                                                            'active': {
                                                                                                lineFill: lineColorRoad,
                                                                                                pointLineFill: lineColorRoad,
                                                                                                polygonTextureFill: lineColorRoad,
                                                                                                lineOpacity: 1,
                                                                                                pointLineOpacity: 1,
                                                                                                lineWidth: [4, 25000, 15, 10000],
                                                                                                labelTemplate: "|roadName|",
                                                                                                hoverTemplate: "|roadName|"
                                                                                            }
                                                                                        }
                                                                                    },
                                                                                    source: {
                                                                                        adhoc: {"features": {},
                                                                                                  "fields": fields}
                                                                                    }
                                                                                },
                                                                                {map: map});
        map.getTree().add(polyLineLayerRoad);

        var layerPanel = new Studio.app.component.layerTree.LayerTree({ map: map }).render();
        // Studio.$(".studio-top.studio-right").html(layerTree.render().el);

        var tabs = new Studio.app.component.panel.tab.Collection([
            new Studio.app.component.panel.tab.Model({
                    id: 'layers',
                    title: 'Layers',
                    iconClass: 'studio-icon studio-layers',
                    view: layerPanel // LayersPanel component
            }),
        ]);

        var toolbar = new Studio.app.control.toolbar.Toolbar({
            collection: tabs,
            margin: false,
            region: "leftBar",
            numFloatBotTab: 0
        });
        map.addControl(toolbar); // Render the toolbar. Use map.removeControl(toolbar) to remove it.
    });
    map.once('fetch:failed', mapFetchFail);
}

/* ---------------------------------*/
/* Function to toggle basemap
/* ---------------------------------*/
export function toggleBaseMap(basemap, active){
    basemap.forEach((m)=>{
        var layer = map.getTree().findByTitle(m);
        layer.setVisible(layer.isVisible() ? false : true);
        layer.setActive(active);
        layer.redraw();
    });
}

export function plotPoint(coords){
    var pointLayer = map.getTree().findByTitle(config.layer.pointLayer);
    var polyLineLayer = map.getTree().findByTitle(config.layer.polyLineLayer);
    var polyLineLayerSoundsLike = map.getTree().findByTitle(config.layer.polyLineLayerSoundsLike);
    var polyLineLayerMaySoundLike = map.getTree().findByTitle(config.layer.polyLineLayerMaySoundLike);
    var polyLineLayerRoad = map.getTree().findByTitle(config.layer.polyLineLayerRoad);
    pointLayer.reload();
    polyLineLayer.reload();
    polyLineLayerSoundsLike.reload();
    polyLineLayerMaySoundLike.reload();
    polyLineLayerRoad.reload();
    // var point = map.createPoint(coords[0], coords[1], pointLayer);
    // point.setCoordinates(coords[0], coords[1]);
    pointLayer.redraw();
}

export function drawLine(_m, _layer, _this){
    var geom = _m.geometry.paths[0][Math.round(_m.geometry.paths[0].length/2)];
    var layer = map.getTree().findByTitle(_layer);
    var _line = map.createPolyline([_this.state.selectedPoint[0], geom[0]],
                                    [_this.state.selectedPoint[1], geom[1]], 2, layer);
    var x = [];
    var y = [];
    var polyLineLayerRoad = map.getTree().findByTitle(config.layer.polyLineLayerRoad);
    _m.geometry.paths[0].forEach((g)=>{
      x.push(g[0]);
      y.push(g[1]);
    });
    var _line2 = map.createPolyline(x, y, _m.geometry.paths[0].length, polyLineLayerRoad);
    layer.redraw();
    polyLineLayerRoad.redraw();
}

export function drawPolygon(geom, l){
    var pointLayer = map.getTree().findByTitle(config.layer.pointLayer);
    var xA = [];
    var yA = [];
    var xC = 0;
    geom.forEach((xd)=>{
        xA.push(xd[0]);
        yA.push(xd[1]);
        xC++;
    });

    if(l === 1){
        var polygon = map.createPolygon(xA, yA, xC, pointLayer);
        pointLayer.redraw();
    }else{
        var polygon = map.createPolygon(xA, yA, xC, pointLayer);
        pointLayer.redraw();
    }
}