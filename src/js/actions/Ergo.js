import * as config from "../config";
import * as ErgoMap from "./ErgoMap";
import * as ErgoEncode from "./ErgoEncode";
import $ from "jquery";
import Stemmer from "stemmer";
import json2csv from "json2csv";

var jenaSettings = {"async": true,
			              "crossDomain": true,
			              "url": config.config.urlJena,
			              "method": "POST",
			              "headers": {
			                "content-type": "application/json",
			                "cache-control": "no-cache"
			              },
			              "processData": false,
			              "data": {ontFile: config.config.urlOwl,
			                        ontPrefix: config.config.ontPrefix,
			                        roadList: null,
			                        phoneticsSettings: null,
			                        roadTypes: null
		                    		}};

export function getMapRoads(_this){
	var searchObj = {
		roads: null,
		suburb: null,
		lga: null,
		bufferGeom: null,
		sequence: [this.getLga, this.getBufferGeom, this.getRoadsByBufferGeom]
	};
	this.getSuburb(_this, searchObj);
}

export function getSuburb(_this, _searchObj){
	if(_this.state.progressObj){
		_this.setState({progressObj: {
			title: "Retrivieng Surrounding Roads",
			message: "Retrivieng surrounding roads from ("
			+ _this.state.selectedPoint[0] + ", "
			+ _this.state.selectedPoint[1] + ") with buffer size of "
			+ _this.state.bufferSize + "km ... retrieving suburb information ..."
		}});
	}
	var formData = new FormData();
	formData.append('where', '1=1');
	formData.append('geometry', JSON.stringify({"x": _this.state.selectedPoint[0], "y": _this.state.selectedPoint[1]}));
	formData.append('geometryType', 'esriGeometryPoint');
	formData.append('inSR', 4326);
	formData.append('outSR', 4326);
	formData.append('spatialRel', 'esriSpatialRelIntersects');
	formData.append('outFields', '*');
	formData.append('returnGeometry', true);
	formData.append('returnTrueCurves', false);
	formData.append('returnIdsOnly', false);
	formData.append('returnCountOnly', false);
	formData.append('returnZ', false);
	formData.append('returnM', false);
	formData.append('returnDistinctValues', false);
	formData.append('f', 'pjson');
	$.ajax({
		url: "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Administrative_Boundaries/MapServer/0/query",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: (response)=>{
			var response = JSON.parse(response);
			// console.log("getSuburb ---> ", response);
			_searchObj.suburb = response.features;
			ErgoMap.drawPolygon(response.features[0].geometry.rings[0], 1);
			if(_searchObj){
				_searchObj.sequence[0](_this, _searchObj);
			}
		},
		error: (request, error, errorthrown)=>{
			console.log("Error: getSuburb : ", request, error, errorthrown);
		},
		async: true
	});
}

export function getLga(_this, _searchObj){
	if(_this.state.progressObj){
		_this.setState({progressObj: {
			title: "Retrivieng Surrounding Roads",
			message: "Retrivieng surrounding roads from ("
			+ _this.state.selectedPoint[0] + ", "
			+ _this.state.selectedPoint[1] + ") with buffer size of "
			+ _this.state.bufferSize + "km ... retrieving LGA boundary ..."
		}});
	}
	var formData = new FormData();
	formData.append('where', '1=1');
	formData.append('geometry', JSON.stringify({"x": _this.state.selectedPoint[0], "y": _this.state.selectedPoint[1]}));
	formData.append('geometryType', 'esriGeometryPoint');
	formData.append('inSR', 4326);
	formData.append('outSR', 4326);
	formData.append('spatialRel', 'esriSpatialRelIntersects');
	formData.append('outFields', '*');
	formData.append('returnGeometry', true);
	formData.append('returnTrueCurves', false);
	formData.append('returnIdsOnly', false);
	formData.append('returnCountOnly', false);
	formData.append('returnZ', false);
	formData.append('returnM', false);
	formData.append('returnDistinctValues', false);
	formData.append('f', 'pjson');
	$.ajax({
		url: "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Administrative_Boundaries/MapServer/1/query",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: (response)=>{
			var response = JSON.parse(response);
			// console.log("getLga ---> ", response);
			_searchObj.lga = response.features;
			if(_searchObj){
				_searchObj.sequence[1](_this, _searchObj);
			}
		},
		error: (request, error, errorthrown)=>{
			console.log("Error: getLga : ", request, error, errorthrown);
		},
		async: true
	});
}

export function getBufferGeom(_this, _searchObj){
	if(_this.state.progressObj){
		_this.setState({progressObj: {
			title: "Retrivieng Surrounding Roads",
			message: "Retrivieng surrounding roads from ("
			+ _this.state.selectedPoint[0] + ", "
			+ _this.state.selectedPoint[1] + ") with buffer size of "
			+ _this.state.bufferSize + "km ... applying buffer to LGA boundary ..."
		}});
	}
	var formData = new FormData();
	formData.append('geometries', JSON.stringify({"geometryType": "esriGeometryPolygon",
													"geometries": [{
															"rings": _searchObj.suburb[0].geometry.rings,
															"spatialReference": {
																"wkid": 4326
															}
														}
													]
												}));
	formData.append('inSR', 4326);
	formData.append('outSR', 4326);
	formData.append('bufferSR', 3308);
	formData.append('distances', _this.state.bufferSize * 1000);
	formData.append('unit', 9001);
	formData.append('unionResults', true);
	formData.append('geodesic', false);
	formData.append('f', 'pjson');
	$.ajax({
		url: "https://maps.six.nsw.gov.au/arcgis/rest/services/Utilities/Geometry/GeometryServer/buffer",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: (response)=>{
			var response = JSON.parse(response);
			_searchObj.bufferGeom = response.geometries[0]
			ErgoMap.drawPolygon(response.geometries[0].rings[0], 2);
			if(_searchObj){
				_searchObj.sequence[2](_this, _searchObj, 0);
			}
		},
		error: (request, error, errorthrown)=>{
			console.log("Error: getBufferGeom : ", request, error, errorthrown);
		},
		async: true
	});
}

var sixRoads = [];
export function getRoadsByBufferGeom(_this, _searchObj, _resultOffset){
	if(_this.state.progressObj){
		_this.setState({progressObj: {
			title: "Retrivieng Surrounding Roads",
			message: "Retrivieng surrounding roads from ("
			+ _this.state.selectedPoint[0] + ", "
			+ _this.state.selectedPoint[1] + ") with buffer size of "
			+ _this.state.bufferSize + "km ... retrieving roads within boundary ..."
		}});
	}
	if(_resultOffset == 0){
		sixRoads = [];
	}
	var formData = new FormData();
	formData.append('where', '1=1');
	formData.append('geometry', JSON.stringify(_searchObj.bufferGeom));
	formData.append('geometryType', 'esriGeometryPolygon');
	formData.append('inSR', 4326);
	formData.append('outSR', 4326);
	formData.append('spatialRel', 'esriSpatialRelIntersects');
	formData.append('outFields', '*');
	formData.append('returnGeometry', true);
	formData.append('returnIdsOnly', false);
	formData.append('returnCountOnly', false);
	formData.append('returnZ', false);
	formData.append('returnM', false);
	formData.append('returnDistinctValues', false);
	formData.append('resultOffset', _resultOffset)
	formData.append('f', 'pjson');
	$.ajax({
		url: "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/16/query",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: (response)=>{
			var response = JSON.parse(response);
			// console.log("getRoadsByBufferGeom ---> ", response);
			if(response && response.features && response.features.length > 0){
				response.features.forEach((r)=>{
					if(r.attributes.roadnamebase && r.attributes.roadnamebase.trim() !== ""){
    				var encodedRoadName = ErgoEncode.encodeRoadName(r.attributes.roadnamebase.trim());
						sixRoads.push({
							roadId: r.attributes.OBJECTID,
							roadnameextentoid: r.attributes.roadnameextentoid,
							requestType: null,
							name: r.attributes.roadnamebase,
							roadName: r.attributes.roadnamestring,
							roadnamesuffix: r.attributes.roadnamesuffix,
							roadType: r.attributes.roadnametype,
							geometry: r.geometry,
              metaphonePrimary: encodedRoadName.metaphonePrimary,
              metaphoneAlternate: encodedRoadName.metaphoneAlternate,
              stem: encodedRoadName.stem,
              stemMetaphonePrimary: encodedRoadName.stemMetaphonePrimary,
              stemMetaphoneAlternate: encodedRoadName.stemMetaphoneAlternate,
              caverphoneOriginal: encodedRoadName.caverphoneOriginal,
              caverphoneRevisited: encodedRoadName.caverphoneRevisited,
              onca: encodedRoadName.onca,
              phonex: encodedRoadName.phonex,
              fuzzySoundex: encodedRoadName.fuzzySoundex,
              sonoripyCount: encodedRoadName.sonoripyCount,
              sonoripyPhonetics: encodedRoadName.sonoripyPhonetics,
              m3Metaph: encodedRoadName.m3Metaph,
              m3MetaphAlt: encodedRoadName.m3MetaphAlt,
              m3MetaphEnEx: encodedRoadName.m3MetaphEnEx,
              m3MetaphEnExAlt: encodedRoadName.m3MetaphEnExAlt,
              m3MetaphEnExVow: encodedRoadName.m3MetaphEnExVow,
              m3MetaphEnExVowAlt: encodedRoadName.m3MetaphEnExVowAlt,
              m3MetaphEnVow: encodedRoadName.m3MetaphEnVow,
              m3MetaphEnVowAlt: encodedRoadName.m3MetaphEnVowAlt
						});
					}
				});
				// Execute qery again if there are more roads
				if(response.exceededTransferLimit){
					_searchObj.sequence[2](_this, _searchObj, (_resultOffset + 1000));
				}else{
					_searchObj.roads = sixRoads;
					_this.storeMapRoads(_searchObj, _this);
				}
			}
		},
		error: (request, error, errorthrown)=>{
			console.log("Error: getRoadsByBufferGeom : ", request, error, errorthrown);
		},
		async: true
	});
}

export function getRoadByNameWithinGeom(_this, _roadObj){
	if(_this.state.progressObj){
		_this.setState({progressObj: {
			title: "Validating Road Names",
			message: "Checking road: " + r.roadName
		}});
	}
	_roadObj.isValid = true;
	_roadObj.outcome = "";
	var formData = new FormData();
	formData.append('where', "roadnamebase='" + _roadObj.roadName.toUpperCase() + "'");
	formData.append('geometry', JSON.stringify(_this.state.roadSearchObj.bufferGeom));
	formData.append('geometryType', 'esriGeometryPolygon');
	formData.append('inSR', 4326);
	formData.append('outSR', 4326);
	formData.append('spatialRel', 'esriSpatialRelIntersects');
	formData.append('outFields', '*');
	formData.append('returnGeometry', true);
	formData.append('returnIdsOnly', false);
	formData.append('returnCountOnly', false);
	formData.append('returnZ', false);
	formData.append('returnM', false);
	formData.append('returnDistinctValues', false);
	formData.append('f', 'pjson');
	$.ajax({
		url: "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/16/query",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: (response)=>{
			var response = JSON.parse(response);
			// console.log("getRoadByNameWithinGeom ---> ", response);
			if(response && response.features && response.features.length === 0){
				_roadObj.isValid = false;
				_roadObj.outcome = _roadObj.roadName.toUpperCase() + " not found within buffer zone."
			}
			_this.consolidateResults(_roadObj, _this);
		},
		error: (request, error, errorthrown)=>{
			console.log("Error: getRoadByNameWithinGeom : ", request, error, errorthrown);
		},
		async: true
	});
}

export function queryJena(roadSearchObj, _this){
	var wsJenaObj = JSON.parse(JSON.stringify(jenaSettings));
	var _roads = roadSearchObj.jenaSearchObj.roadList;
	wsJenaObj.data.phoneticsSettings = roadSearchObj.jenaSearchObj.phoneticsSettings;
	wsJenaObj.data.roadTypes = roadSearchObj.jenaSearchObj.roadTypes;
	if(roadSearchObj.jenaSearchObj.mapRoads.length > 1000){
		var i, j, batch = [], temparray, _response, chunk = 1000;
		for (i=0,j=roadSearchObj.jenaSearchObj.mapRoads.length; i<j; i+=chunk) {
	    temparray = roadSearchObj.jenaSearchObj.mapRoads.slice(i,i+chunk);
	    temparray.push.apply(temparray, roadSearchObj.jenaSearchObj.proposedRoads);
	    batch.push(temparray);
		}
		qJ(0);
		function qJ(_count){
			if(_count > 0){
				wsJenaObj.data = JSON.parse(wsJenaObj.data);
			}
			wsJenaObj.data.roadList = batch[_count];
			wsJenaObj.data = JSON.stringify(wsJenaObj.data);
			$.ajax(wsJenaObj).done((jResponse)=>{
				_count++;
				if(!_response){
					_response = jResponse;
				}else{
					_response.forEach((r)=>{
						jResponse.forEach((_r)=>{
							if(r.road.roadId.toString() == _r.road.roadId.toString()){
								if(_r.maySoundLikeRoads){
									if(r.maySoundLikeRoads){
										var ex = false;
										_r.maySoundLikeRoads.forEach((__r)=>{
											r.maySoundLikeRoads.forEach((___r)=>{
												if(__r.roadId.toString() == ___r.roadId.toString()){
													ex = true;
													return;
												}
											});
											if(!ex){
												r.maySoundLikeRoads.push(__r);
											}
										});
									}else{
										r.maySoundLikeRoads = _r.maySoundLikeRoads;
									}
								}
								if(_r.sameNameRoads){
									if(r.sameNameRoads){
										var ex = false;
										_r.sameNameRoads.forEach((__r)=>{
											r.sameNameRoads.forEach((___r)=>{
												if(__r.roadId.toString() == ___r.roadId.toString()){
													ex = true;
													return;
												}
											});
											if(!ex){
												r.sameNameRoads.push(__r);
											}
										});
									}else{
										r.sameNameRoads = _r.sameNameRoads;
									}
								}
								if(_r.soundsLikeRoads){
									if(r.soundsLikeRoads){
										var ex = false;
										_r.soundsLikeRoads.forEach((__r)=>{
											r.soundsLikeRoads.forEach((___r)=>{
												if(__r.roadId.toString() == ___r.roadId.toString()){
													ex = true;
													return;
												}
											});
											if(!ex){
												r.soundsLikeRoads.push(__r);
											}
										});
									}else{
										r.soundsLikeRoads = _r.soundsLikeRoads;
									}
								}
								if(_r.outcome != ""){
									r.outcome = _r.outcome;
								}
							}
						});
					});
				}
				if(_count == batch.length){
					roadSearchObj.jenaResponse = _response;
					_this.consolidateResults(roadSearchObj, _this);
				}else{
					qJ(_count);
				}
			});
		}
	}else{
		wsJenaObj.data.roadList = roadSearchObj.jenaSearchObj.roadList;
		wsJenaObj.data = JSON.stringify(wsJenaObj.data);
		$.ajax(wsJenaObj).done((jResponse)=>{
			roadSearchObj.isFinal = true;
			roadSearchObj.jenaResponse = jResponse;
			_this.consolidateResults(roadSearchObj, _this);
		});
	}
}

export function onDownload(_this){
	var array = [];
	var includedCols = ["requestType", "roadName", "roadType", "accessType"];
	var fields = ["rowId"]
	var proposedRoads = JSON.parse(JSON.stringify(_this.state.proposedRoadList));
	proposedRoads.forEach((row)=>{
		var obj = {};
		var message = "";
		for (var prop in row) {
			// Create fields array for CSV header
			if(includedCols.indexOf(prop) >= 0){
				if(fields.indexOf(prop) < 0){
					fields.push(prop);
				}
				obj[prop] = row[prop];
			}
		}
		fields.push("result");
		if(obj.roadType){
			obj.roadType = row.roadType.text;
		}
		if(row.sameNameRoads && row.sameNameRoads.length > 0){
			message += "Same Name As: ";
			row.sameNameRoads.forEach((r)=>{
				message += r.roadName + "(" + r.distance + "); ";
			});
		}
		if(row.soundsLikeRoads && row.soundsLikeRoads.length > 0){
			message += "Sounds Like: ";
			row.soundsLikeRoads.forEach((r)=>{
				message += r.roadName + "(" + r.distance + ") [" + r.matchedOn + "]; ";
			});
		}
		if(row.maySoundLikeRoads && row.maySoundLikeRoads.length > 0){
			message += "May Sounds Like: ";
			row.maySoundLikeRoads.forEach((r)=>{
				message += r.roadName + "(" + r.distance + ") [" + r.matchedOn + "]; ";
			});
		}
		message += row.outcome;
		obj["result"] = message === "" ? "VALID" : message;
		array.push(obj);
	});
	var csv = json2csv({ data: array, fields: fields });
	var aLink = document.createElement('a');
	var event = new MouseEvent('click');
	aLink.download = "Report.csv";
	aLink.href = 'data:text/csv;charset=UTF-8,' + encodeURIComponent(csv);
	aLink.dispatchEvent(event);
}