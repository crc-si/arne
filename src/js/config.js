var envProd = true;
var envLive = false;
var urlOwl = "file://" + (envLive ?
      "/home/developer/owl/arne.owl" :
      "/V:/z.prj CRC/NSW/GNB/owl/arne.owl");
var ontPrefix = "http://www.semanticweb.org/ontologies/arne#";

export var config = {
    apiKey: envProd ?
	    	"AStBVkkwTUNXVE5reWNQbXJQMmdObC9QajhSQklLdFNiQ2FWQ0xzSXhQRkZnQtG94wjrdns0OfoG8IyVKC59kZK1W6+as5noiehjVYQ9S2HVpw" :
	    	"AStBVkkwTUNXVE5reWNQbXJQMmdObC9QajhSQklLdFNiQ2FWQ0xzSXhQRkZnQtG94wjrdns0OfoG8IyVKC59kZK1W6+as5noiehjVYQ9S2HVpw",
    map: envProd ? "AV3lPAA25O1I2zhYNfxg" : "AV3lPAA25O1I2zhYNfxg",
    mapworksPath: "https://app" + (envProd ?  "" : ".stage") + ".mapworks.io/api/v1",
    mapPath: "https://api" + (envProd ?  "" : ".stage") + ".mapworks.io/maps/latest",
    bingKey: "AiRX6VGPHOMmLeG2b3QWw0Ii_2u1qQKcTrEdaANgrjabX_1HWJ1brFTqia_4mNBz",
    urlJena: envLive ?
      "https://crcsi.amristar.com/jenaws/arneSearch" :
			"http://localhost:8080/jenaWs/arneSearch",
    urlOwl: urlOwl,
  	ontPrefix: ontPrefix,
};

export var bufferList = [
  {text: "Metropolitan (10km)", value: 10},
  {text: "Regional (20km)", value: 20},
  {text: "Rural (30km)", value: 30},
  {text: "Remote (50km)", value: 50}];

export var layer = {
  pointLayer: "Point",
  polyLineLayer: "Roads with Same Name",
  polyLineLayerSoundsLike: "Roads with Similar Sound",
  polyLineLayerMaySoundLike: "Roads with Possible Similar Sound",
  polyLineLayerRoad: "Road",
};