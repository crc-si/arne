import React from "react";
import Footer from "../components/Footer";
import Nav from "../components/Nav";
import * as config from "../config";
import * as Ergo from "../actions/Ergo";
import * as ErgoMap from "../actions/ErgoMap";
import * as ErgoEncode from "../actions/ErgoEncode";
import LookupStore from "../stores/LookupStore";
import Geolib from "geolib";
import parse from "csv-parse";

export default class Layout extends React.Component {
  constructor(props, context) {
    super();
    this.state = {
      startTime: null,
      isShowProgress: false,
      progressObj: null,
      selectedPoint: ["", ""],
      bufferList: config.bufferList,
      bufferSize: config.bufferList[0].value,
      isShowRoadSubmit: false,
      isShowUpload: false,
      isShowSettings: false,
      mapShow: {isShowSixMap: true, isShowMapworks: false},
      proposedRoadList: [{rowId: 0,
                          requestTypeList: LookupStore.getRequestType(),
                          roadTypeList: LookupStore.getRoadType(),
                          accessTypeList: LookupStore.getAccessType("Null"),
                          requestType: "",
                          roadNameFrom: "",
                          roadName: "",
                          roadType: {"value": "Null", "text": ""},
                          accessType: "",
                          name: "",
                          open: false,
                          outcome: "",
                          isValid: null,
                          sameNameRoads: [],
                          soundsLikeRoads: [],
                          maySoundLikeRoads: []}],
      roadSearchObj: {},
      phoneticsSettings: {
        isMetaphone3: true,
        isDoubleMetaphone: true,
        isCaverphone: true,
        isOnca: true,
        isFuzzySoundex: true,
        isPhonex: false,
        isSonoripy: false
      },
      errorMessage: "",
      modal: {title: "",
              message: "",
              show: false}
    };
  }

  componentDidMount(){
  }

  handleUpdateMapShow(mapShow){
    if(mapShow){
      this.setState({mapShow: mapShow});
    }else{
      var obj = {
        isShowSixMap: this.state.mapShow.isShowSixMap ? false : true,
        isShowMapworks: this.state.mapShow.isShowMapworks ? false : true,
      };
      this.setState({bufferSize: obj});
    }
  }

  handleBufferChange(buffer){
    this.setState({bufferSize: buffer});
  }

  preSixSearch(){
    this.setState({startTime: new Date().getTime()});
    this.setState({roadSearchObj: {
      mapRoads: null,
      suburb: null,
      lga: null,
      jenaSearchObj: null,
      jenaResponseObj: null,
      sixMaptime: null,
      time: null
    }});
    this.setState({progressObj: {
      title: "Retrivieng Surrounding Roads",
      message: "Retrivieng surrounding roads from ("
              + this.state.selectedPoint[0] + ", "
              + this.state.selectedPoint[1] + ") with buffer size of "
              + this.state.bufferSize + "km"
    }});
    this.setState({isShowProgress: true});
  }

  handleUpdateSelectedPoint(selectedPoint, isMapworks){
    this.setState({selectedPoint: selectedPoint});
    if(isMapworks){
      this.preSixSearch();
      ErgoMap.plotPoint(selectedPoint);
      Ergo.getMapRoads(this);
    }
  }

  handleShowProgress(){
    this.setState({isShowProgress: this.state.isShowProgress ? false : true});
  }

  handleGetSurroundingRoads(){
    this.preSixSearch();
    Ergo.getMapRoads(this);
  }

  storeMapRoads(_searchObj, _this){
    var roadSearchObj = JSON.parse(JSON.stringify(_this.state.roadSearchObj));
    roadSearchObj = {
      mapRoads: _searchObj.roads,
      suburb: _searchObj.suburb,
      lga: _searchObj.lga,
      jenaSearchObj: null,
      jenaResponseObj: null,
      sixMaptime: ((end - _this.state.startTime)/ 1000).toFixed(2),
      bufferGeom: _searchObj.bufferGeom,
      time: null
    };
    var end = new Date().getTime();
    this.setState({progressObj: null});
    _this.setState({isShowProgress: false});
    _this.setState({roadSearchObj: roadSearchObj});
  }

  handleValidate() {
    var roadSearchObj = JSON.parse(JSON.stringify(this.state.roadSearchObj));
    var proposedRoadList = JSON.parse(JSON.stringify(this.state.proposedRoadList));
    if(roadSearchObj.mapRoads && roadSearchObj.mapRoads.length > 0){
      this.setState({startTime: new Date().getTime()});
      this.setState({progressObj: {
        title: "Validating Roads",
        message: "Validating "
                + this.state.proposedRoadList.length + " proposed roads against "
                + roadSearchObj.mapRoads.length + " surrounding roads."
      }});
      this.setState({isShowProgress: true});

      var jenaSearchObj = {
        selectedDistance: this.state.bufferSize,
        startTime: new Date().getTime(),
        endTime: null,
        jenaValidateTime: null,
        jenaErrorCount: 0,
        roadList: [],
        mapRoads: [],
        proposedRoads: [],
        tempSearchArray: [],
        phoneticsSettings: this.state.phoneticsSettings,
        roadTypes: LookupStore.getRoadType()
      };

      roadSearchObj.mapRoads.forEach((r)=>{
        var _obj = {
          proposalType: "SURROUNDING",
          roadId: r.roadId,
          nameFrom: "",
          name: r.name,
          roadName: r.roadName,
          roadType: r.roadType,
          accessType: "",
          metaphonePrimary: r.metaphonePrimary,
          metaphoneAlternate: r.metaphoneAlternate,
          stem: r.stem,
          stemMetaphonePrimary: r.stemMetaphonePrimary,
          stemMetaphoneAlternate: r.stemMetaphoneAlternate,
          caverphoneOriginal: r.caverphoneOriginal,
          caverphoneRevisited: r.caverphoneRevisited,
          onca: r.onca,
          fuzzySoundex: r.fuzzySoundex,
          phonex: r.phonex,
          sonoripyCount: r.sonoripyCount,
          sonoripyPhonetics: r.sonoripyPhonetics,
          m3Metaph: r.m3Metaph,
          m3MetaphAlt: r.m3MetaphAlt,
          m3MetaphEnEx: r.m3MetaphEnEx,
          m3MetaphEnExAlt: r.m3MetaphEnExAlt,
          m3MetaphEnExVow: r.m3MetaphEnExVow,
          m3MetaphEnExVowAlt: r.m3MetaphEnExVowAlt,
          m3MetaphEnVow: r.m3MetaphEnVow,
          m3MetaphEnVowAlt: r.m3MetaphEnVowAlt,
          isProposedRoad: "0"
        };
        jenaSearchObj.roadList.push(_obj);
        jenaSearchObj.mapRoads.push(_obj);
      });

      proposedRoadList.forEach((r)=>{
        r.isValid = null;
        r.sameNameRoads = [];
        r.soundsLikeRoads = [];
        r.maySoundLikeRoads = [];
        r.outcome = "";
        if(r.requestType.toUpperCase() !== "EXTEND EXISTING"){
          var _obj = {
            proposalType: r.requestType.toUpperCase(),
            roadId: r.rowId,
            nameFrom: r.roadNameFrom.toUpperCase(),
            name: r.roadName.toUpperCase(),
            roadName: r.roadName.toUpperCase() + " " + r.roadType.text.toUpperCase(),
            roadType: r.roadType.text.toUpperCase(),
            accessType: r.accessType.toUpperCase(),
            metaphonePrimary: r.metaphonePrimary,
            metaphoneAlternate: r.metaphoneAlternate,
            stem: r.stem,
            stemMetaphonePrimary: r.stemMetaphonePrimary,
            stemMetaphoneAlternate: r.stemMetaphoneAlternate,
            caverphoneOriginal: r.caverphoneOriginal,
            caverphoneRevisited: r.caverphoneRevisited,
            onca: r.onca,
            fuzzySoundex: r.fuzzySoundex,
            phonex: r.phonex,
            sonoripyCount: r.sonoripyCount,
            sonoripyPhonetics: r.sonoripyPhonetics,
            m3Metaph: r.m3Metaph,
            m3MetaphAlt: r.m3MetaphAlt,
            m3MetaphEnEx: r.m3MetaphEnEx,
            m3MetaphEnExAlt: r.m3MetaphEnExAlt,
            m3MetaphEnExVow: r.m3MetaphEnExVow,
            m3MetaphEnExVowAlt: r.m3MetaphEnExVowAlt,
            m3MetaphEnVow: r.m3MetaphEnVow,
            m3MetaphEnVowAlt: r.m3MetaphEnVowAlt,
            isProposedRoad: "1"
          };
          jenaSearchObj.roadList.push(_obj);
          jenaSearchObj.proposedRoads.push(_obj);
        }else{
          Ergo.getRoadByNameWithinGeom(this, r);
        }
      });
      if(jenaSearchObj.roadList.length > roadSearchObj.mapRoads.length){
        roadSearchObj.jenaSearchObj = jenaSearchObj;
        roadSearchObj.jenaResponseObj = null;
        this.setState({roadSearchObj: roadSearchObj});
        Ergo.queryJena(roadSearchObj, this);
      }
    }
  }

  handleRoadTypeUpate(road, newObj){
    this.handleUpdateRow(road, newObj, LookupStore.getAccessType(newObj.roadType.value));
  }

  handleAddRow() {
    var tempArray = JSON.parse(JSON.stringify(this.state.proposedRoadList));
    // Maximum number of roads to evaluate
    if(tempArray.length <= 50){
      if(!this.state.isShowUpload){
        tempArray.push({rowId: tempArray.length,
                        requestTypeList: LookupStore.getRequestType(),
                        roadTypeList: LookupStore.getRoadType(),
                        accessTypeList: LookupStore.getAccessType("Null"),
                        requestType: "",
                        roadNameFrom: "",
                        roadName: "",
                        roadType: {"value": "Null", "text": ""},
                        accessType: "",
                        isValid: null,
                        outcome: "",
                        sameNameRoads: [],
                        soundsLikeRoads: [],
                        maySoundLikeRoads: [],
                        metaphonePrimary: null,
                        metaphoneAlternate: null,
                        stem: null,
                        stemMetaphonePrimary: null,
                        stemMetaphoneAlternate: null,
                        caverphoneOriginal: null,
                        caverphoneRevisited: null,
                        onca: null,
                        fuzzySoundex: null,
                        phonex: null,
                        sonoripyCount: null,
                        sonoripyPhonetics: null,
                        m3Metaph: null,
                        m3MetaphAlt: null,
                        m3MetaphEnEx: null,
                        m3MetaphEnExAlt: null,
                        m3MetaphEnExVow: null,
                        m3MetaphEnExVowAlt: null,
                        m3MetaphEnVow: null,
                        m3MetaphEnVowAlt: null});
        this.setState({proposedRoadList: tempArray});
      }
      this.setState({isShowUpload: false});
      this.validateRoadNames(tempArray);
    }else{
      this.setState({modal: {title: "Opps!",
                            message: "There are too many roads to evaluate. Please limit the maximum number of roads to 50.",
                            show: true}});
      return false;
    }
  }

  handleRemoveRow(road) {
    var index = -1;
    var tempArray = JSON.parse(JSON.stringify(this.state.proposedRoadList));
    // Minimum number of roads to evaluate
    if(tempArray.length > 1){
      for( var i = 0; i < tempArray.length; i++ ) {
        if(tempArray[i].rowId === road.rowId) {
          index = i;
          break;
        }
      }
      tempArray.splice(index, 1);
      this.validateRoadNames(tempArray);
      this.setState({proposedRoadList: tempArray});
    }
  }

  handleUpdateRow(road, newObj, newAccessTypeList) {
    var tempArray = JSON.parse(JSON.stringify(this.state.proposedRoadList));

    for( var i = 0; i < tempArray.length; i++ ) {
      if(tempArray[i].rowId === road.rowId) {
        var encodedRoadName = ErgoEncode.encodeRoadName(newObj.roadName.trim());

        tempArray[i].rowId = road.rowId;
        tempArray[i].requestType = newObj.requestType;
        tempArray[i].roadNameFrom = newObj.roadNameFrom;
        tempArray[i].roadName = newObj.roadName;
        tempArray[i].roadType = newObj.roadType;
        tempArray[i].accessType = newObj.accessType;
        tempArray[i].isValid = ErgoEncode.regEx(newObj.roadName);
        tempArray[i].metaphonePrimary = encodedRoadName.metaphonePrimary;
        tempArray[i].metaphoneAlternate = encodedRoadName.metaphoneAlternate;
        tempArray[i].stem = encodedRoadName.stem;
        tempArray[i].stemMetaphonePrimary = encodedRoadName.stemMetaphonePrimary;
        tempArray[i].stemMetaphoneAlternate = encodedRoadName.stemMetaphoneAlternate;
        tempArray[i].caverphoneOriginal = encodedRoadName.caverphoneOriginal;
        tempArray[i].caverphoneRevisited = encodedRoadName.caverphoneRevisited;
        tempArray[i].onca = encodedRoadName.onca;
        tempArray[i].phonex = encodedRoadName.phonex;
        tempArray[i].fuzzySoundex = encodedRoadName.fuzzySoundex;
        tempArray[i].sonoripyCount = encodedRoadName.sonoripyCount;
        tempArray[i].sonoripyPhonetics = encodedRoadName.sonoripyPhonetics;
        tempArray[i].m3Metaph = encodedRoadName.m3Metaph;
        tempArray[i].m3MetaphAlt = encodedRoadName.m3MetaphAlt;
        tempArray[i].m3MetaphEnEx = encodedRoadName.m3MetaphEnEx;
        tempArray[i].m3MetaphEnExAlt = encodedRoadName.m3MetaphEnExAlt;
        tempArray[i].m3MetaphEnExVow = encodedRoadName.m3MetaphEnExVow;
        tempArray[i].m3MetaphEnExVowAlt = encodedRoadName.m3MetaphEnExVowAlt;
        tempArray[i].m3MetaphEnVow = encodedRoadName.m3MetaphEnVow;
        tempArray[i].m3MetaphEnVowAlt = encodedRoadName.m3MetaphEnVowAlt;

        // Update Access Type if update triggered by Road Type change
        if(newAccessTypeList){
          tempArray[i].accessType = newAccessTypeList[0].text;
          tempArray[i].accessTypeList = newAccessTypeList;
        }
        if(newObj.accessType === "Null"){
          tempArray[i].accessTypeList = [];
        }
      }
    }
    this.validateRoadNames(tempArray);
  }

  handleShowUpload(isShow){
    this.setState({isShowUpload: isShow});
  }

  handleOnDrop(files){
    files.forEach((file)=>{
        const reader = new FileReader();
        reader.onload = () => {
            const csv = reader.result;
            parse(csv.replace(/"/g, ""), {delimiter: ',', skip_empty_lines: true, relax_column_count: true, from: 2}, (err, output)=>{
              var tempArray = [];
              var counter = 0;
              output.forEach((r)=>{
                if(r[1].trim() != ""){
                  var encodedRoadName = ErgoEncode.encodeRoadName(r[1].trim());
                  tempArray.push({rowId: counter,
                                  requestTypeList: LookupStore.getRequestType(),
                                  roadTypeList: LookupStore.getRoadType(),
                                  requestType: r[0],
                                  roadNameFrom: "",
                                  roadName: r[1].trim(),
                                  roadType: {"value": r[2], "text": r[2]},
                                  accessType: r[3],
                                  accessTypeList: LookupStore.getAccessType(r[2]),
                                  isValid: null,
                                  metaphonePrimary: encodedRoadName.metaphonePrimary,
                                  metaphoneAlternate: encodedRoadName.metaphoneAlternate,
                                  stem: encodedRoadName.stem,
                                  stemMetaphonePrimary: encodedRoadName.stemMetaphonePrimary,
                                  stemMetaphoneAlternate: encodedRoadName.stemMetaphoneAlternate,
                                  caverphoneOriginal: encodedRoadName.caverphoneOriginal,
                                  caverphoneRevisited: encodedRoadName.caverphoneRevisited,
                                  onca: encodedRoadName.onca,
                                  phonex: encodedRoadName.phonex,
                                  fuzzySoundex: encodedRoadName.fuzzySoundex,
                                  sonoripyCount: encodedRoadName.sonoripyCount,
                                  sonoripyPhonetics: encodedRoadName.sonoripyPhonetics,
                                  m3Metaph: encodedRoadName.m3Metaph,
                                  m3MetaphAlt: encodedRoadName.m3MetaphAlt,
                                  m3MetaphEnEx: encodedRoadName.m3MetaphEnEx,
                                  m3MetaphEnExAlt: encodedRoadName.m3MetaphEnExAlt,
                                  m3MetaphEnExVow: encodedRoadName.m3MetaphEnExVow,
                                  m3MetaphEnExVowAlt: encodedRoadName.m3MetaphEnExVowAlt,
                                  m3MetaphEnVow: encodedRoadName.m3MetaphEnVow,
                                  m3MetaphEnVowAlt: encodedRoadName.m3MetaphEnVowAlt
                                })
                  counter++;
                }
              });
              this.setState({isShowUpload: false});
              this.validateRoadNames(tempArray);
            });
        };
        reader.onabort = () => console.log('file reading was aborted');
        reader.onerror = () => console.log('file reading has failed');
        reader.readAsBinaryString(file);
    });
  }

  handleShowRoad(rowId){
    var tempArray = JSON.parse(JSON.stringify(this.state.proposedRoadList));
    tempArray.forEach((i)=>{
      if(i.rowId.toString() === rowId.toString()){
        i.open = i.open ? false : true;
      }
    });
    this.setState({proposedRoadList: tempArray});
  }

  validateRoadNames(tempArray){
    var metaPhoneList = [];
    var uNames = [];
    var duplicateList = [];
    var badNamesList = [];
    var similarNamesList = [];
    var isValid = true;

    tempArray.forEach((r)=>{
      r.isValid = null;
      r.outcome = "";
      if((r.requestType.toUpperCase() === "CHANGE" && r.roadNameFrom.trim() === "")
        || r.roadName === ""
        || r.roadType.value === "Null"
        || r.accessType === "Null"){
        isValid = false;
      }else{
        var nFrom = r.roadNameFrom && r.roadNameFrom.trim() !== "" ? r.roadNameFrom.toUpperCase().trim() : null;
        var n = r.roadName.toUpperCase().trim();
        // Check if road names are unique
        if(uNames.indexOf(n) < 0){
          uNames.push(n);
        }else{
          r.isValid = false;
          isValid = false;
          if(duplicateList.indexOf(n) < 0){
            duplicateList.push(n);
          }
        }
        // // Check if road names are unique
        // var mp = CaverphoneOriginal(n);
        // var ma = CaverphoneRevisited(n);
        // var soundsLike = "";
        // var found = metaPhoneList.some((metn)=>{
        //   if(metn.mp === mp || metn.ma === mp
        //     || metn.ma === ma || metn.mp === ma){
        //     soundsLike = metn.name;
        //     return true;
        //   }
        // });
        // if(n !== "" && found){
        //   r.isValid = false;
        //   isValid = false;
        //   r.outcome = n + " sounds like " + soundsLike;
        // }else{
        //   metaPhoneList.push({name: n, mp: mp, ma: ma});
        // }
        // Check if road name is only text
        if(!ErgoEncode.regEx(n)){
          if(n.indexOf("'") > 0 && n.toLowerCase().indexOf("'s") <= 0){
            return;
          }
          r.isValid = false;
          isValid = false;
          r.outcome = "(Principle 6.7.2) " + n + " contains invalid characters.";
          if(badNamesList.indexOf(n) < 0){
            badNamesList.push(n);
          }
        }
        if(nFrom && !ErgoEncode.regEx(nFrom)){
          if(nFrom.indexOf("'") > 0 && nFrom.toLowerCase().indexOf("'s") <= 0){
            return;
          }
          r.isValid = false;
          isValid = false;
          r.outcome = "(Principle 6.7.2) " + n + " contains invalid characters.";
        }
      }
    });
    if(!isValid){
      if(duplicateList.length > 0){
        this.setState({duplicateMsg: "There are duplicate road names submitted:"});
        this.setState({duplicateList});
      }
      if(badNamesList.length > 0){
        this.setState({badNamesMsg: "There are invalid road names submitted:"});
        this.setState({badNamesList});
      }
    }
    this.setState({proposedRoadList: tempArray});
    this.setState({isShowRoadSubmit: isValid});
    return isValid;
  }

  consolidateResults(roadSearchObj, _this){
    var proposedRoadList = JSON.parse(JSON.stringify(_this.state.proposedRoadList));
    if(roadSearchObj.rowId !== null && parseInt(roadSearchObj.rowId) >= 0){
      proposedRoadList.forEach((pr)=>{
        if(roadSearchObj.rowId.toString() === pr.rowId.toString()){
          pr.isValid = roadSearchObj.isValid;
          pr.outcome = roadSearchObj.outcome;
        }
      });
    }
    if(roadSearchObj.jenaResponse && roadSearchObj.jenaResponse.length > 0){
      proposedRoadList.forEach((pr)=>{
        pr.isValid = true;
        pr.sameNameRoads = [];
        pr.soundsLikeRoads = [];
        pr.maySoundLikeRoads = [];
        pr.outcome = "";
        roadSearchObj.jenaResponse.forEach((jr)=>{
          if(jr.road.roadId.toString() === pr.rowId.toString()
              && ((jr.maySoundLikeRoads && jr.maySoundLikeRoads.length > 0)
                || (jr.sameNameRoads && jr.sameNameRoads.length > 0)
                || (jr.soundsLikeRoads && jr.soundsLikeRoads.length > 0) || jr.outcome)){
            pr.isValid = false;
            // Process Same Name Roads
            if(jr.sameNameRoads && jr.sameNameRoads.length > 0){
              jr.sameNameRoads.forEach((x)=>{
                roadSearchObj.mapRoads.forEach((m)=>{
                  if(x.roadId.toString() === m.roadId.toString()){
                    m["distance"] = getDistance(m) + "km";
                    m["location"] = m.geometry.paths[0][Math.round(m.geometry.paths[0].length/2)];
                    pr.sameNameRoads.push(m);
                    ErgoMap.drawLine(m, config.layer.polyLineLayer, _this);
                  }
                });
              });
            }
            // Process Sounds Like Roads
            if(jr.soundsLikeRoads && jr.soundsLikeRoads.length > 0){
              jr.soundsLikeRoads.forEach((x)=>{
                roadSearchObj.mapRoads.forEach((m)=>{
                  if(x.roadId.toString() === m.roadId.toString()){
                    m["distance"] = getDistance(m) + "km";
                    m["location"] = m.geometry.paths[0][Math.round(m.geometry.paths[0].length/2)];
                    m["matchedOn"] = x.matchedOn;
                    pr.soundsLikeRoads.push(m);
                    ErgoMap.drawLine(m, config.layer.polyLineLayerSoundsLike, _this);
                  }
                });
              });
            }
            // Process May Sound Like Roads
            if(jr.maySoundLikeRoads && jr.maySoundLikeRoads.length > 0){
              jr.maySoundLikeRoads.forEach((x)=>{
                roadSearchObj.mapRoads.forEach((m)=>{
                  if(x.roadId.toString() === m.roadId.toString()){
                    m["distance"] = getDistance(m) + "km";
                    m["location"] = m.geometry.paths[0][Math.round(m.geometry.paths[0].length/2)];
                    m["matchedOn"] = x.matchedOn;
                    pr.maySoundLikeRoads.push(m);
                    ErgoMap.drawLine(m, config.layer.polyLineLayerMaySoundLike, _this);
                  }
                });
              });
            }
            pr.outcome = jr.outcome ? jr.outcome : "";

            function getDistance(_m){
              var geom = _m.geometry.paths[0][Math.round(_m.geometry.paths[0].length/2)];
              var distance = (Geolib.getDistance(
                              {latitude: geom[0], longitude: geom[1]},
                              {latitude: _this.state.selectedPoint[0], longitude: _this.state.selectedPoint[1]}) / 1000).toFixed(2);
              return distance;
            }
          }
        });
      });
    }
    var end = new Date().getTime();
    roadSearchObj.time = ((end - _this.state.startTime)/ 1000).toFixed(2);
    _this.setState({progressObj: null});
    _this.setState({isShowProgress: false});
    _this.setState({proposedRoadList: proposedRoadList});
    // console.log("FINAL ===> ",  roadSearchObj);
  }

  handleToggleModal(){
    this.setState({isShowSettings: this.state.isShowSettings ? false : true});
  }

  handleUpdatePhoneticsAlgorithm(e){
    var obj = JSON.parse(JSON.stringify(this.state.phoneticsSettings));
    switch(e) {
      case 'Metaphone3':
        obj.isMetaphone3 = this.state.phoneticsSettings.isMetaphone3 ? false : true;
      break;
      case 'DoubleMetaphone':
        obj.isDoubleMetaphone = this.state.phoneticsSettings.isDoubleMetaphone ? false : true;
      break;
      case 'Caverphone':
        obj.isCaverphone = this.state.phoneticsSettings.isCaverphone ? false : true;
      break;
      case 'Onca':
        obj.isOnca = this.state.phoneticsSettings.isOnca ? false : true;
      break;
      case 'FuzzySoundex':
        obj.isFuzzySoundex = this.state.phoneticsSettings.isFuzzySoundex ? false : true;
      break;
      case 'Phonex':
        obj.isPhonex = this.state.phoneticsSettings.isPhonex ? false : true;
      break;
      case 'Sonoripy':
        obj.isSonoripy = this.state.phoneticsSettings.isSonoripy ? false : true;
      break;
    }
    var errorMessage = "Select at least 1 phonetics algorithm";
    if(obj.isMetaphone3 || obj.isDoubleMetaphone || obj.isCaverphone || obj.isOnca || obj.isFuzzySoundex || obj.isPhonex || obj.isSonoripy){
      errorMessage = "";
    }
    this.setState({errorMessage: errorMessage});
    this.setState({phoneticsSettings: obj});
  }

  handleDownload(){
    Ergo.onDownload(this);
  }

  handleZoomToRoad(road){
    map.setViewCenter(road.location[0], road.location[1], 5000);
  }

  render() {
    const { location } = this.props;
    return (
      <div>
        <Nav location={location} />
        <div>
            {React.cloneElement(this.props.children,
              {bufferSize: this.state.bufferSize,
                bufferList: this.state.bufferList,
                selectedPoint: this.state.selectedPoint,
                isShowRoadSubmit: this.state.isShowRoadSubmit,
                isShowUpload: this.state.isShowUpload,
                isShowSettings: this.state.isShowSettings,
                mapShow: this.state.mapShow,
                roadSearchObj: this.state.roadSearchObj,
                proposedRoadList: this.state.proposedRoadList,
                isShowProgress: this.state.isShowProgress,
                progressObj: this.state.progressObj,
                phoneticsSettings: this.state.phoneticsSettings,
                errorMessage: this.state.errorMessage,
                onShowProgress: this.handleShowProgress.bind(this),
                onBufferChange: this.handleBufferChange.bind(this),
                onUpdateSelectedPoint: this.handleUpdateSelectedPoint.bind(this),
                onGetSurroundingRoads: this.handleGetSurroundingRoads.bind(this),
                onValidate: this.handleValidate.bind(this),
                onAddRow: this.handleAddRow.bind(this),
                onRoadTypeUpdate: this.handleRoadTypeUpate.bind(this),
                onRemoveRow: this.handleRemoveRow.bind(this),
                onUpdateRow: this.handleUpdateRow.bind(this),
                onShowRoad: this.handleShowRoad.bind(this),
                onUpdateMapShow: this.handleUpdateMapShow.bind(this),
                onDownload: this.handleDownload.bind(this),
                onShowUpload: this.handleShowUpload.bind(this),
                onDrop: this.handleOnDrop.bind(this),
                onZoomToRoad: this.handleZoomToRoad.bind(this),
                onToggleModal: this.handleToggleModal.bind(this),
                onUpdatePhoneticsAlgorithm: this.handleUpdatePhoneticsAlgorithm.bind(this)
              }
            )}
        </div>
      </div>
    );
  }
}
