import React from "react";
import ReactDOM from "react-dom";
import * as Ergo from "../actions/Ergo";
import $ from "jquery";
import RoadList from "../components/RoadList";
import Settings from "../components/Settings";
import Progress from "../components/Progress";
import Map from "../components/Map";
import { Popover, OverlayTrigger } from 'react-bootstrap';

export default class Main extends React.Component {
	constructor(){
		super();
	}

	onUpdateSelectedPoint(){
		if((!isNaN(this.refs.longitude.value) || this.refs.longitude.value === "")
			&& (!isNaN(this.refs.lattitude.value || this.refs.lattitude.value === ""))){
			this.props.onUpdateSelectedPoint([this.refs.longitude.value, this.refs.lattitude.value]);
		}
	}

	onBufferChange(){
		this.props.onBufferChange(this.refs.bufferSize.value);
	}

	render() {
		var mainStyle = {
			padding: "0px",
			margin: "0px",
			height: "90vh",
			width: "100%"
		}
	    var dis = {
	      display: 'inline-block',
	      paddingRight: '10px'
	    };
	    var options = [];
	    this.props.bufferList.forEach((b)=>{
	    	options.push(<option key={b.text} value={b.value}>{b.text}</option>);
	    });
		return (
			<div style={mainStyle}>
				{this.props.progressObj ?
      			<Progress showProgress={this.props.isShowProgress}
			                progressObj={this.props.progressObj}
			                closeModal={this.props.onShowProgress} /> : null}
				<div class="col-lg-6">
	        <div class="col-lg-12">
		        	<h3 style={dis}>Select Development Site</h3>
				      <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="Select Development Site">Select a divelopment site from the map or enter the coordinates below.</Popover>}>
				      	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
							</OverlayTrigger>
							<div class="col-lg-12">
	                        <div class="col-lg-3">Longitude:</div>
			        		<div class="col-lg-3">Lattitude:</div>
			        		<div class="col-lg-3">Buffer:</div>
			        		<div class="col-lg-3">&nbsp;</div>
			        		<div class="col-lg-3">
		                        <input class="form-control input-sm"
				                        id="longitude"
				                        type="text"
				                        placeholder="Longitude e.g. 149.581077"
				                        ref="longitude"
				                        onChange={this.onUpdateSelectedPoint.bind(this)}
				                        value={this.props.selectedPoint[0]}/>
	                        </div>
			        		<div class="col-lg-3">
				        		<input class="form-control input-sm"
				                        id="lattitude"
				                        type="text"
				                        placeholder="Lattitude e.g. -33.414956"
				                        ref="lattitude"
				                        onChange={this.onUpdateSelectedPoint.bind(this)}
				                        value={this.props.selectedPoint[1]}/>
	                        </div>
	                        <div class="col-lg-3">
														<select class="form-control input-sm" id="select" ref="bufferSize" onChange={this.onBufferChange.bind(this)}>
			                        {options}
		                        </select>
	                        </div>
	                        <div class="col-lg-3">
	                        	{!this.props.isProcessing && this.props.selectedPoint[0] !== "" && this.props.selectedPoint[1] !== "" ?
	                        		<button type="submit" onClick={()=>{this.props.onGetSurroundingRoads()}} class="form-control btn-primary input-sm">Next</button> : null}
	                        </div>
	                        {this.props.roadSearchObj && this.props.roadSearchObj.mapRoads
	                        	? <div class="col-lg-12">Retrieved&nbsp;
	                        	<b>{this.props.roadSearchObj.mapRoads.length}</b>&nbsp;roads in&nbsp;
	                        	<b>{this.props.roadSearchObj.suburb[0].attributes.suburbname}</b>,&nbsp;
	                        	<b>{this.props.roadSearchObj.lga[0].attributes.lganame}</b>.
	                        	</div> : null}
		        		</div>
          		</div>
	        		{this.props.selectedPoint
	        			&& this.props.selectedPoint.length > 0
	        			&& this.props.selectedPoint[0] !== ""
	        			&& this.props.selectedPoint[1] !== "" ?
        			<RoadList proposedRoadList={this.props.proposedRoadList}
	            					onRoadTypeUpdate={this.props.onRoadTypeUpdate}
				        				onRemoveRow={this.props.onRemoveRow}
				        				onUpdateRow={this.props.onUpdateRow}
				        				onAddRow={this.props.onAddRow}
	            					onShowRoad={this.props.onShowRoad}
	            					onZoomToRoad={this.props.onZoomToRoad}
	            					onValidate={this.props.onValidate}
	            					roadSearchObj={this.props.roadSearchObj}
	            					onDownload={this.props.onDownload}
	            					errorMessage={this.props.errorMessage}
	            					onShowUpload={this.props.onShowUpload}
	            					isShowUpload={this.props.isShowUpload}
	            					onDrop={this.props.onDrop}
			                  onToggleModal={this.props.onToggleModal}
	            					isShowRoadSubmit={this.props.isShowRoadSubmit} /> : null}
				</div>
				<div class="col-lg-6">
			        <div class="col-lg-12" style={mainStyle}>
						<Map mapShow={this.props.mapShow}
								onUpdateMapShow={this.props.onUpdateMapShow}
								onUpdateSelectedPoint={this.props.onUpdateSelectedPoint} />
					</div>
				</div>
        <Settings isShowSettings={this.props.isShowSettings}
                  phoneticsSettings={this.props.phoneticsSettings}
                  onToggleModal={this.props.onToggleModal}
                  onUpdatePhoneticsAlgorithm={this.props.onUpdatePhoneticsAlgorithm}
                  errorMessage={this.props.errorMessage} />
			</div>
		);
	}
}