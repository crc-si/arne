import React from "react";

export default class Nav extends React.Component {
  constructor(props, context) {
    super();
    this.state = {
      collapsed: true,
    };
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {
    var nav = {
      marginBottom: "0px"
    };
    var logoMargin = {
      height: "25px",
      margin: "10px"
    };
    var buttonMargin = {
      margin: "10px"
    };
    var logo = "../../.." + window.location.pathname + "images/logo.png";

    return (
      <nav class="navbar navbar-default" role="navigation" style={nav}>
        <div className="container-fluid">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle" onClick={this.toggleCollapse.bind(this)}>
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand" href="./"><img src={logo} height="150%"></img></a>
            <a className="navbar-brand" href="./"><strong>ARNE</strong> | Automated Road Name Evaluation</a>
          </div>
        </div>
      </nav>
    );
  }
}