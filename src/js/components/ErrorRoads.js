import React from "react";
import {Collapse} from 'react-bootstrap';
import ExRoad from "./ExRoad";

export default class ErrorRoads extends React.Component {
  render() {
    var errorRoadsStyle = {
      padding: 0,
      border: 'none',
      borderWidth: 'none',
    };
    var styleSmallText = {
        fontSize: "80%"}
    var _existingRoads = [];
    if(this.props.road.sameNameRoads){
      this.props.road.sameNameRoads.forEach((eR)=>{
        _existingRoads.push(<ExRoad key={_existingRoads.length + "_" + eR.roadId} road={eR} onZoomToRoad={this.props.onZoomToRoad} isSoundsLike={false} />);
      });
    }
    if(this.props.road.soundsLikeRoads){
      this.props.road.soundsLikeRoads.forEach((eR)=>{
        _existingRoads.push(<ExRoad key={_existingRoads.length + "_" + eR.roadId} road={eR} onZoomToRoad={this.props.onZoomToRoad} isSoundsLike={true} />);
      });
    }
    if(this.props.road.maySoundLikeRoads){
      this.props.road.maySoundLikeRoads.forEach((eR)=>{
        _existingRoads.push(<ExRoad key={_existingRoads.length + "_" + eR.roadId} road={eR} onZoomToRoad={this.props.onZoomToRoad} isSoundsLike={true} />);
      });
    }
    return (
      <tr>
        <td colSpan="5">
          <Collapse in={this.props.road.open}>
            <div>
              {this.props.road.outcome !== "" ?
                <span style={styleSmallText}><i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                &nbsp;&nbsp;&nbsp;{this.props.road.outcome}<br/></span> : null}
              {_existingRoads}
            </div>
          </Collapse>
        </td>
      </tr>
    );
  }
}
