import React from "react";

export default class Spinner extends React.Component {
  render() {
	var divStyleSpinner = {
		textAlign: "center",
		fontSize: "50px"
	};
    return (
      <div style={divStyleSpinner}><i class="fa fa-spinner fa-spin"></i></div>
    );
  }
}