import React from "react";
import {Modal, OverlayTrigger, Popover} from 'react-bootstrap';

export default class Settings extends React.Component {
	constructor(props, context) {
		super();
	}

	onPhoneticsCheck(e){
		this.props.onUpdatePhoneticsAlgorithm(e);
	}

	render() {
		const {phoneticsSettings} = this.props;
		return (
			<Modal show={this.props.isShowSettings}
						animation={true}
						backdrop={this.props.errorMessage == "" ? true : "static"}
						onHide={this.props.onToggleModal}
						keyboard={this.props.errorMessage == ""}
						bsSize="large">
				<Modal.Header>
					<Modal.Title>
						<div class="form-group">
							<div class="col-lg-10">
								Phonetics Evaluation Algorithm
							</div>
							<div class="col-lg-2 text-right">
								{this.props.errorMessage == "" ? <a href="#" onClick={this.props.onToggleModal}><i class="fa fa-times-circle" aria-hidden="true"></i></a> : null }
							</div>
						</div>
					</Modal.Title>
				</Modal.Header>
				<Modal.Body class="col-lg-12 form-group">
					<div class="checkbox">
					  <label><input type="checkbox" checked={phoneticsSettings.isMetaphone3} onChange={this.onPhoneticsCheck.bind(this, "Metaphone3")} /><a href="#" onClick={this.onPhoneticsCheck.bind(this, "Metaphone3")}>Metaphone 3</a></label>
					  &nbsp;&nbsp;&nbsp;
				    <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="Metaphone 3">
				    	Metaphone 3 further improves phonetic encoding of words in the English language, non-English words familiar to Americans, and first names and family names commonly found in the United States.</Popover>}>
				    	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
						</OverlayTrigger>
					  &nbsp;&nbsp;&nbsp;<a href="https://en.wikipedia.org/wiki/Metaphone#Metaphone_3" target="_blank"><i class="fa fa-external-link"></i></a>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" checked={phoneticsSettings.isDoubleMetaphone} onChange={this.onPhoneticsCheck.bind(this, "DoubleMetaphone")} /><a href="#" onClick={this.onPhoneticsCheck.bind(this, "DoubleMetaphone")}>Double Metaphone</a></label>
					  &nbsp;&nbsp;&nbsp;
				    <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="Double Metaphone">
				    	The double metaphone algorithm, created in 2000 by Lawrence Philips, is an improvement over the original metaphone algorithm.</Popover>}>
				    	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
						</OverlayTrigger>
					  &nbsp;&nbsp;&nbsp;<a href="https://en.wikipedia.org/wiki/Metaphone" target="_blank"><i class="fa fa-external-link"></i></a>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" checked={phoneticsSettings.isCaverphone} onChange={this.onPhoneticsCheck.bind(this, "Caverphone")} /><a href="#" onClick={this.onPhoneticsCheck.bind(this, "Caverphone")}>Caverphone</a></label>
					  &nbsp;&nbsp;&nbsp;
				    <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="Caverphone">
				    	The caverphone algorithm, written by David Hood for the Caversham project, aims at encoding names and specifically targeting names from New Zealand.</Popover>}>
				    	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
						</OverlayTrigger>
					  &nbsp;&nbsp;&nbsp;<a href="https://en.wikipedia.org/wiki/Caverphone" target="_blank"><i class="fa fa-external-link"></i></a>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" checked={phoneticsSettings.isOnca} onChange={this.onPhoneticsCheck.bind(this, "Onca")} /><a href="#" onClick={this.onPhoneticsCheck.bind(this, "Onca")}>Onca</a></label>
					  &nbsp;&nbsp;&nbsp;
				    <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="The Oxford Name Compression Algorithm (ONCA)">Basically a glorified combination of the NYSIIS algorithm & the Soundex one.</Popover>}>
				    	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
						</OverlayTrigger>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" checked={phoneticsSettings.isFuzzySoundex} onChange={this.onPhoneticsCheck.bind(this, "FuzzySoundex")} /><a href="#" onClick={this.onPhoneticsCheck.bind(this, "FuzzySoundex")}>Fuzzy Soundex</a></label>
					  &nbsp;&nbsp;&nbsp;
				    <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="Fuzzy-Soundex">
				    	This algorithm is designed as an improvement over the classical Soundex.<br/><br/>
				    	This improvement is achieved by performing some substitutions in the style of what the NYSIIS algorithm does, plus fuzzying some name beginnings & endings.</Popover>}>
				    	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
						</OverlayTrigger>
					  &nbsp;&nbsp;&nbsp;<a href="http://wayback.archive.org/web/20100629121128/http://www.ir.iit.edu/publications/downloads/IEEESoundexV5.pdf" target="_blank"><i class="fa fa-external-link"></i></a>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" checked={phoneticsSettings.isPhonex} onChange={this.onPhoneticsCheck.bind(this, "Phonex")} /><a href="#" onClick={this.onPhoneticsCheck.bind(this, "Phonex")}>Phonex</a></label>
					  &nbsp;&nbsp;&nbsp;
				    <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="Phonex">
				    	This algorithm is an improved version of the Soundex algorithm.<br/><br/>
				    	Its main change is to better fuzz some very common cases missed by the Soundex algorithm in order to match more orthographic variations.</Popover>}>
				    	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
						</OverlayTrigger>
					  &nbsp;&nbsp;&nbsp;<a href="http://homepages.cs.ncl.ac.uk/brian.randell/Genealogy/NameMatching.pdf" target="_blank"><i class="fa fa-external-link"></i></a>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" checked={phoneticsSettings.isSonoripy} onChange={this.onPhoneticsCheck.bind(this, "Sonoripy")} /><a href="#" onClick={this.onPhoneticsCheck.bind(this, "Sonoripy")}>Sonoripy</a></label>
					  &nbsp;&nbsp;&nbsp;
				    <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="SonoriPy">
					    SonoriPy is a language-independent syllabification algorithm following the Sonority Sequencing Principle.<br/><br/>
					    As opposed to LegaliPy, this algorithm doesn’t need to be trained on word tokens but must instead be provided with the target language’s sonority hierarchy.<br/><br/>
							Hence, note that the default tokenizer provided by this library is configured with a rough English sonority hierarchy but that you can create a custom tokenizer using your own hierarchy if needed.</Popover>}>
				    	<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
						</OverlayTrigger>
					</div>
				</Modal.Body>
				<Modal.Footer>
					<span class="text-primary">{this.props.errorMessage}&nbsp;&nbsp;&nbsp;</span>
					{this.props.errorMessage == "" ? <a href="#" class="btn btn-default" onClick={this.props.onToggleModal}>Close</a> : null }
				</Modal.Footer>
			</Modal>
		);
	}
}