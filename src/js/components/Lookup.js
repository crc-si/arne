import React from "react";

export default class Lookup extends React.Component {
  render() {
    const {id, text} = this.props;

    return (
        <option value={id} key={id}>{text}</option>
      );
    }
  }