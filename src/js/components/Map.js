import React from "react";
import * as ErgoMap from "../actions/ErgoMap";

export default class Map extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {
    ErgoMap.loadMapworks();
    map.once('ready', ()=>{
      // this.props.onUpdateMapShow({isShowSixMap: true, isShowMapworks: false});
      map.on("feature:mouseclick", (event)=>{
        if (map.getControl('tooltip')) {
          map.getControl('tooltip').destroyTooltip();
        }
        // this.props.onShowProgress();
        var coords = map.getCoordinates(event.getX(), event.getY(), false);
        this.props.onUpdateSelectedPoint([coords[0], coords[1]], true);
      });
    });
  }

  onUpdateMapShow(map){
    this.props.onUpdateMapShow({isShowSixMap: (map==0), isShowMapworks: (map==1)});
  }

  render() {
    const mapStyles = {
      width: "100%",
      height: "100%"
    };
    const SixMapImgStyle = {
      filter: "grayscale(" + (this.props.mapShow.isShowSixMap ? "0" : "100") + "%)",
      opacity: (this.props.mapShow.isShowSixMap ? "1" : "0.5")
    };
    const MapworksImgStyle = {
      filter: "grayscale(" + (this.props.mapShow.isShowMapworks ? "0" : "100") + "%)",
      opacity: (this.props.mapShow.isShowMapworks ? "1" : "0.5")
    };
    const SixMapStyle = {
      width: "100%",
      height: "100%",
      display: this.props.mapShow.isShowSixMap ? "inline-block" : "none"
    };
    const MapworksStyle = {
      width: "100%",
      height: "100%",
      display: this.props.mapShow.isShowMapworks ? "inline-block" : "none"
    };
    return (
      <div class="col-lg-12" style={mapStyles}>
        <a href="#" style={SixMapImgStyle} onClick={this.onUpdateMapShow.bind(this, 0)}><img src="images/SixMapsLogo.png" height="25px"></img></a>
         &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
        <a href="#" style={MapworksImgStyle} onClick={this.onUpdateMapShow.bind(this, 1)}><img src="images/MapworksLogo.png" height="25px"></img></a>
        <iframe style={SixMapStyle} src="https://maps.six.nsw.gov.au/" name="iframe_a"></iframe>
        <div id="map" style={MapworksStyle}></div>
      </div>
    );
  }
}