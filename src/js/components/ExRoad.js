import React from "react";
import {OverlayTrigger, Tooltip} from 'react-bootstrap';

export default class ExRoad extends React.Component {
  render() {
    var styleSmallText = {fontSize: "80%"}
    return (
	  	<a href="#" onClick={()=>{this.props.onZoomToRoad(this.props.road)}}>
  			<OverlayTrigger placement="top" overlay={<Tooltip id="ttExRoad">{this.props.isSoundsLike ? "Sounds Like" : "Same Name As"}</Tooltip>}>
					<span style={styleSmallText}>
						<i class={this.props.isSoundsLike ? "fa fa-assistive-listening-systems" : "fa fa-road"} aria-hidden="true"></i>
	      		&nbsp;&nbsp;&nbsp;{this.props.isSoundsLike ? "" : <span>(Principle 6.7.4)&nbsp;&nbsp;&nbsp;</span>}{this.props.road.roadName} -> {this.props.road.distance}
	      		{this.props.isSoundsLike ? <span>&nbsp;&nbsp;&nbsp; ({this.props.road.matchedOn}) </span> : null}
	      		<br/>
	    		</span>
				</OverlayTrigger>
			</a>
    );
  }
}