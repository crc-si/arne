import React from "react";
import {Collapse, OverlayTrigger, Tooltip} from 'react-bootstrap';
import Lookup from "./Lookup";
// import ExRoad from "./ExRoad";

export default class Road extends React.Component {
  handleRequestTypeChange(e){
    this.syncAllFields();
  }
  handleRoadNameChange(e){
    this.syncAllFields();
  }
  handleRoadTypeChange(e){
    this.handleUpdateRoadType();
    // this.syncAllFields();
  }
  handleAccessTypeChange(e){
    this.syncAllFields();
  }
  syncAllFields(){
    this.props.onUpdateRow(this.props.road, this.getAllFieds());
  }
  getAllFieds(){
    return  {requestType: this.refs.requestType.value,
              roadNameFrom: (this.refs.roadNameFrom ? this.refs.roadNameFrom.value : ""),
              roadName: this.refs.roadName.value,
              roadType: {"value": this.refs.roadType.value, "text": this.refs.roadType.options[this.refs.roadType.selectedIndex].text},
              accessType: this.refs.accessType.value};
  }
  //  ------------------------------------------------
  handleUpdateRoadType() {
    this.props.onRoadTypeUpdate(this.props.road, this.getAllFieds());
    return false;
  }
  handleClearRoad(e) {
    e.preventDefault();
    this.props.onUpdateRow(this.props.road, {requestType: "New",
                                              roadNameFrom: "",
                                              roadName: "",
                                              roadNameErrorMsg: "",
                                              roadType: {"value": "Null", "text": ""},
                                              accessType: "Null",
                                              accessTypeList: [],
                                              isValid: false});
    return false;
  }
  handleShowRoad(e) {
    e.preventDefault();
    this.props.onShowRoad(this.props.road.rowId);
    return false;
  }
  handleRemoveRoad(e) {
    e.preventDefault();
    this.props.onRemoveRow(this.props.road);
    return false;
  }
  onAddRow(e){
    if(e.keyCode === 9){
      this.props.onAddRow();
    }
  }
  render() {
    var disInline = {
      width: "100%",
      paddingLeft: '10px'
    };
    var styleError = {
      color: "#C71C22"
    };
    var tbxStyle = this.props.road.isValid === null || this.props.road.isValid === true ||  this.props.road.roadName === ""
                    ? "form-group" : "form-group has-error";
    const requestTypeList = this.props.road.requestTypeList.map((v)=>{
      return <Lookup key={v.id} id={v.id} {...v}/>;
    });
    const roadTypeList = this.props.road.roadTypeList.map((v)=>{
      return <Lookup key={v.id} id={v.id} {...v}/>;
    });
    var accessTypeList = <Lookup key={""} id={""} value={""}/>;
    if(this.props.road && this.props.road.accessTypeList){
      accessTypeList = this.props.road.accessTypeList.map((v)=>{
        return <Lookup key={v.id} id={v.id} {...v}/>;
      });
    }
    return (
      <tr>
        <td>
          <select class="form-control input-sm" id="selectRequestType" onChange={this.handleRequestTypeChange.bind(this)} value={this.props.road.requestType} ref="requestType">
            {requestTypeList}
          </select>
        </td>
        <td>
            <div class={tbxStyle} style={disInline}>
              {this.props.road.requestType.toUpperCase() === "CHANGE" ?
              <input class="form-control input-sm"
                      id="inputRoadNameFrom"
                      type="text"
                      placeholder="Change Road Name FROM..."
                      onChange={this.handleRoadNameChange.bind(this)}
                      value={this.props.road.roadNameFrom} ref="roadNameFrom"/> : null}
              <input class="form-control input-sm"
                      id="inputRoadName"
                      type="text"
                      placeholder={this.props.road.requestType.toUpperCase() === "CHANGE" ? "Change Road Name TO..." : "Road Name"}
                      onChange={this.handleRoadNameChange.bind(this)}
                      value={this.props.road.roadName} ref="roadName"/>
            </div>
        </td>
        <td>
          <select class="form-control input-sm" id="selectRoadType" onChange={this.handleRoadTypeChange.bind(this)} value={this.props.road.roadType.value} ref="roadType">
            {roadTypeList}
          </select>
        </td>
        <td>
          <select class="form-control input-sm" id="selectAccessType" onKeyDown={this.onAddRow.bind(this)} onChange={this.handleAccessTypeChange.bind(this)} value={this.props.road.accessType} ref="accessType">
            {accessTypeList}
          </select>
        </td>
        <td>
          <a href="" onClick={this.handleClearRoad.bind(this)}>
            <OverlayTrigger placement="top" overlay={<Tooltip id="ttRemoveRoadOnMap">Remove road on map.</Tooltip>}>
              <i class="fa fa-eraser" aria-hidden="true"></i>
            </OverlayTrigger>
          </a>&nbsp;&nbsp;&nbsp;
          <a href="" onClick={this.handleRemoveRoad.bind(this)}>
            <OverlayTrigger placement="top" overlay={<Tooltip id="ttDeleteROad">Delete road.</Tooltip>}>
              <i class="fa fa-trash" aria-hidden="true"></i>
            </OverlayTrigger>
          </a>&nbsp;&nbsp;&nbsp;
          {(this.props.road.sameNameRoads && this.props.road.sameNameRoads.length > 0)
          || (this.props.road.soundsLikeRoads && this.props.road.soundsLikeRoads.length > 0)
          || (this.props.road.maySoundLikeRoads && this.props.road.maySoundLikeRoads.length > 0)
          || this.props.road.outcome !== "" ?
          <a href="" onClick={this.handleShowRoad.bind(this)}>
            <OverlayTrigger placement="top" overlay={<Tooltip id="ttDeleteROad">View errors.</Tooltip>}>
              <i class="fa fa-exclamation-circle" style={{color: 'red'}} aria-hidden="true"></i>
            </OverlayTrigger>
          </a> : null}
          {this.props.road.isValid ? <i class="fa fa-check-circle" style={{color: 'green'}} aria-hidden="true"></i> : null}
        </td>
      </tr>
    );
  }
}
