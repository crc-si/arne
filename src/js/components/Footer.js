import React from "react";

export default class Footer extends React.Component {
  render() {
    const footerStyles = {
      textAlign : "center"
    };

    return (
      <footer style={footerStyles}>
        <div>
          <div class="col-sm-12">
            <p><strong>&copy; GNB | CRCSI 2017</strong></p>
          </div>
        </div>
      </footer>
    );
  }
}