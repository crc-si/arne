import React from "react";
import Road from "./Road";
import ErrorRoads from "./ErrorRoads";
import Dropzone from "react-dropzone";
import { Popover, OverlayTrigger, Button, Tooltip } from 'react-bootstrap';

export default class RoadList extends React.Component {
  render() {
    var dis = {
      display: 'inline-block',
      paddingRight: '10px'
    };
    var dropStyle = {
      width: '100%',
      height: '200px',
      border: 'grey',
      borderWidth: '1px',
      borderStyle: 'dotted'
    };
    var roads = [];
    if(this.props.proposedRoadList){
      this.props.proposedRoadList.forEach((road)=>{
        roads.push(
          <Road key = {road.rowId}
                road = {road}
                onRoadTypeUpdate = {this.props.onRoadTypeUpdate}
                onRemoveRow = {this.props.onRemoveRow}
                onUpdateRow = {this.props.onUpdateRow}
                onAddRow = {this.props.onAddRow}
                onShowRoad = {this.props.onShowRoad}
                onZoomToRoad={this.props.onZoomToRoad} />);

        if((road.sameNameRoads && road.sameNameRoads.length > 0)
          || (road.soundsLikeRoads && road.soundsLikeRoads.length > 0)
          || (road.maySoundLikeRoads && road.maySoundLikeRoads.length > 0)
          || road.outcome !== ""){
          roads.push(
            <ErrorRoads key = {"error_" + road.rowId}
                  road = {road}
                  onShowRoad = {this.props.onShowRoad}
                  onZoomToRoad={this.props.onZoomToRoad} />);
        }
      });
    }
    return (
      <div>
        <div class="col-lg-12">
          <h3 style={dis}>Proposed Road Names</h3>
          <OverlayTrigger trigger="click" rootClose placement="right" overlay={<Popover id="2" title="Proposed Road Names">Enter road names below to query their suitability.</Popover>}>
            <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i>
          </OverlayTrigger>
          {this.props.roadSearchObj.jenaResponse ?
          <p>Validated <b>{this.props.proposedRoadList.length}</b> in <b>{this.props.roadSearchObj.time}</b> seconds.</p> : null}
          <h4>
            <OverlayTrigger placement="top" overlay={<Tooltip id="ttAddProposedRoad">Add Proposed Road</Tooltip>}>
              <i class="fa fa-plus-square" aria-hidden="true" onClick={()=>{this.props.onAddRow()}}></i>
            </OverlayTrigger>
            &nbsp;&nbsp;&nbsp;
            <OverlayTrigger placement="top" overlay={<Tooltip id="ttAddProposedRoad">Upload List of Roads</Tooltip>}>
              <i class="fa fa-upload" aria-hidden="true" onClick={()=>{this.props.onShowUpload(true)}} data-toggle="tooltip" data-placement="right"></i>
            </OverlayTrigger>
          </h4>
          {!this.props.isShowUpload ?
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Request Type</th>
                <th>Road Name</th>
                <th>Road Type</th>
                <th>Access Type</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {roads}
            </tbody>
          </table> : null}
          {this.props.isShowUpload ?
          <div class="col-lg-12 dropzone">
            <Dropzone style={dropStyle} onDrop={this.props.onDrop}>
              <p>Drop a file with a list of proposed road names or click to select a file to upload.</p>
            </Dropzone>
          </div> : null}
        </div>
        {!this.props.isShowUpload ?
        <div class="col-lg-12 text-right">
          {this.props.isShowRoadSubmit && this.props.roadSearchObj.jenaResponse ?
            <button class="btn" onClick={this.props.onDownload}><i class="fa fa-download" aria-hidden="true"></i></button>
            : null}
          {this.props.isShowRoadSubmit ?
            <div class="btn-group">
              <button type="submit" class="btn btn-primary" onClick={this.props.onValidate}>Evaluate Proposed Road Name{this.props.proposedRoadList.length > 1 ? "s" : ""}</button>
              <button type="button" class="btn btn-primary" onClick={this.props.onToggleModal} data-toggle="tooltip" data-placement="top" title="Phonetics Evaluation Algorithm"><i class="fa fa-cog" aria-hidden="true"></i></button>
            </div>
          : null}
        </div> : null}
      </div>
    );
  }
}