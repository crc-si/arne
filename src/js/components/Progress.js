import React from "react";
import {Modal, ProgressBar} from 'react-bootstrap';

export default class Progress extends React.Component {
  render() {
    return (
		<Modal show={this.props.showProgress} animation={true} backdrop="static" bsSize="lg">
			<Modal.Header>
				<Modal.Title>
					<div class="form-group">
						<div class="col-lg-10">
							{this.props.progressObj.title}
						</div>
					</div>
				</Modal.Title>
			</Modal.Header>
			<Modal.Body class="col-lg-12">
				<i class="fa fa-spinner fa-spin"/>&nbsp;&nbsp;&nbsp;{this.props.progressObj.message}
			</Modal.Body>
			<Modal.Footer>
			</Modal.Footer>
		</Modal>
    );
  }
}