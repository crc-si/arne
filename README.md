# Setup
    cd [project folder]

    npm install

### Development Environment
    npm run dev

Access via localhost:8080

### Production Environment

    webpack -p

Start a http-server in ./src/

Access application at index.html